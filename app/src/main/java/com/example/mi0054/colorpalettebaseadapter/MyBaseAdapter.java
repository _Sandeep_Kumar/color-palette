package com.example.mi0054.colorpalettebaseadapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by MI0054 on 4/8/2015.
 * Custom base class to inflate the list view.
 */
public class MyBaseAdapter extends BaseAdapter {

    ArrayList<ListData> myList = new ArrayList<ListData>();
    LayoutInflater inflater;
    Context context;


    public MyBaseAdapter(Context context, ArrayList myList) {
        this.myList = myList;
        this.context = context;
        inflater = LayoutInflater.from(this.context);
    }

    @Override
    public int getCount() {
        return myList.size();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public ListData getItem(int position) {
        return (ListData) myList.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        MyViewHolder mViewHolder;

        if(convertView == null) {
            convertView = inflater.inflate(R.layout.layout_list_item, null);
            mViewHolder = new MyViewHolder();
            convertView.setTag(mViewHolder);
        } else {
            mViewHolder = (MyViewHolder) convertView.getTag();
        }

        mViewHolder.tvTitle = detail(convertView, R.id.tvTitle, myList.get(position).getTitle());
        // mViewHolder.tvDesc = detail(convertView, R.id.tvDesc, myList.get(position).getDescription());
        mViewHolder.ivIcon = detail(convertView, R.id.blackClicked, myList.get(position).getImgResId());

        if( ListData.lockedRowsFromColorPalette[position] == position){
            mViewHolder.ivIcon.setVisibility(View.VISIBLE);
        }

        if(ListData.colorChanges[position] == "Black"){
            mViewHolder.tvTitle.setTextColor(Color.parseColor("#000000"));
            mViewHolder.ivIcon.setImageResource(R.drawable.block);
        }

        if(ListData.colorChanges[position] == "White"){
            mViewHolder.tvTitle.setTextColor(Color.parseColor("#FFFFFF"));
            mViewHolder.ivIcon.setImageResource(R.drawable.wlock);
        }

        convertView.setBackgroundColor(Color.parseColor(mViewHolder.tvTitle.getText().toString()));

        return convertView;

    }

    private TextView detail(View v, int resId, String text) {
        TextView tv = (TextView) v.findViewById(resId);
        tv.setText(text);
        return tv;
    }

    private ImageView detail(View v, int resId, int icon) {
        ImageView ic = (ImageView) v.findViewById(resId);
        ic.setImageResource(R.drawable.block);
        return ic;
    }

    private class MyViewHolder {
        TextView tvTitle;
        ImageView ivIcon;
    }

}
