package com.example.mi0054.colorpalettebaseadapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class ColorPaletteBaseAdapter extends ActionBarActivity {

    ListView lvDetail;
    Context context = ColorPaletteBaseAdapter.this;
    ArrayList myList = new ArrayList();
    ImageView image;
    View viewToChange;
    BaseAdapter newAdapter;

    String [] selectedColors = new String[5];

    String[] title = new String[] {
            "#2176AE", "#57B8FF", "#B66D0D", "#FBB13C", "#FE6847"/*, "#EFAAC4", "#7389BA", "#00BCD4", "#C0CA33", "#1565C0", "#009688", "#FFAB00"*/
    };

    int[] icons = new int[] {R.drawable.block};

//    String[] desc = new String[] {
//        "Desc 1", "Desc 2", "Desc 3"
//    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_color_palette_base_adapter);

        for (int i=0; i<5; i++) {
            ListData.lockedRowsFromColorPalette[i] = 9;
        }

        lvDetail = (ListView) findViewById(R.id.lvCustomList);
        getDataInList(title);
        newAdapter = new MyBaseAdapter(context, myList);
        lvDetail.setAdapter(newAdapter);

        /*Activate the selection mode on list*/
        lvDetail.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        lvDetail.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                ImageView image = (ImageView) view.findViewById(R.id.blackClicked);

                if(image.getVisibility() == View.VISIBLE){
                    image.setVisibility(View.INVISIBLE);
                    selectedColors[position] = null;
                    ListData.lockedRowsFromColorPalette[position] = 9;
                } else {
                    image.setVisibility(View.VISIBLE);
                    selectedColors[position] = title[position];
                    ListData.lockedRowsFromColorPalette[position] = position;

//                    ListView clickedList = (ListView) view.findViewById(R.id.lvCustomList);
//                    selectedColors[position] = String.valueOf(Integer.parseInt(title[position]));
//                    Toast.makeText(getApplicationContext(), selectedColors[position], Toast.LENGTH_SHORT).show();
                }

//                Toast.makeText(getApplicationContext(), String.valueOf(selectedColors[position]), Toast.LENGTH_SHORT).show();

//                "clicked at position : " + ((Integer) position).toString()

            }
        });

        Button fetchColors = (Button) findViewById(R.id.footer);

        fetchColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                Toast.makeText(getApplicationContext(), "Fetching colors " , Toast.LENGTH_SHORT).show();

//                title = new String[] {
//                         "#EFAAC4", "#7389BA", "#00BCD4", "#C0CA33", "#1565C0", "#009688", "#FFAB00"
//                };

                for(int i=0; i<5; i++) {
                    if (selectedColors[i] != null){
                        title[i] = selectedColors[i];
                    } else {
                        Random rnd = new Random();
                        String  color = "#"+Integer.toHexString(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))).substring(2);

                        int r = Integer.parseInt(color.substring(1,3), 16) * 299;
                        int g = Integer.parseInt(color.substring(3,5), 16) * 587;
                        int b = Integer.parseInt(color.substring(5,7), 16) * 114;
                        int result = (r + g + b)/1000;

                        if(result > 128){
                            ListData.colorChanges[i] = "Black";
                        } else {
                            ListData.colorChanges[i] = "White";
                        }

                        title[i] = color.toUpperCase();
                    }
                }

                getDataInList(title);

                newAdapter.notifyDataSetChanged();
                lvDetail.setAdapter(newAdapter);

//                for(int i=0; i<selectedColors.length; i++) {
//                    selectedColors[i] = null;
//                }

            }
        });

    }

    private void getDataInList(String[] title) {
        myList.clear();
        for(int i=0; i<title.length; i++) {
            ListData ld = new ListData();
            ld.setTitle(title[i]);
            ld.setImgResId(icons[0]);
//            ld.setDescription(desc[i]);
            myList.add(ld);
        }
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_color_palette_base_adapter, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
