package com.example.mi0054.colorpalettebaseadapter;

import android.widget.ImageView;

/**
 * Created by MI0054 on 4/8/2015.
 * Getter and setter for populating list item
 */
public class ListData {

    //    String Description;
    String Title;
    int imgResId;
    public static int[] lockedRowsFromColorPalette = new int[5];
    public static String[] colorChanges = new String[5];

    public int getImgResId() {
        return imgResId;
    }

    public void setImgResId(int imgResId) {
        this.imgResId = imgResId;
    }

//    public void setDescription(String description) {
//        Description = description;
//    }

//    public String getDescription() {
//        return Description ;
//    }



    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

}
